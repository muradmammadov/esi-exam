package com.example.demo.sales.application.dto;

import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class POUpdateDTO {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate updateEndDate;
    LocalDate updateStartDate;
    PlantInventoryItem item;
}