package com.example.demo.sales;


import com.example.demo.DemoApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@WebAppConfiguration
@DirtiesContext
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RESTTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void poDoesntExist() throws Exception{

        mockMvc.perform(put("/orders/0/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{"+
                        "}"))
                .andDo(print())
                .andExpect(status().is(409))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
    }


    @Test
    public void poUpdated() throws Exception{

        mockMvc.perform(put("/orders/1/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"comment\": \"comment\",\n" +
                        "  \"entryId\": \"1\",\n" +
                        "  \"entryName\": \"exc\",\n" +
                        "  \"nameOfConstructionSite\": \"a\",\n" +
                        "  \"nameOfSiteEngineer\": \"a\",\n" +
                        "  \"rentalPeriod\": {\n" +
                        "    \"endDate\": \"2019-06-28\",\n" +
                        "    \"startDate\": \"2019-06-29\"\n" +
                        "  }\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
    }


}
